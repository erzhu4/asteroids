var sum = function (){
  var argArray = Array.prototype.slice.call(arguments);
  var total = 0;
  for (var i =0; i < argArray.length; i++){
    total += argArray[i];
  }
  return total;
};

Function.prototype.myBind = function (obj){
  var fn = this;
  var argArr = Array.prototype.slice.call(arguments, 1);
  return function (){
    var innerArgs = Array.prototype.slice.call(arguments);
    fn.apply(obj, argArr.concat(innerArgs));
  };
};

var curriedSum = function(numArgs){
  var numbers = [];

  return function _curriedSum (num){
    numbers.push(num)

    if (numbers.length === numArgs){
      var total = 0;
      for (var i = 0 ; i < numbers.length; i++){
        total += numbers[i];
      }

      return total;
    }
    else{
      return _curriedSum;
    }
  };
};

Function.prototype.curry = function(numArgs){
  var args = [];
  var fn = this;

  return function _curry (arg){

    args.push(arg);
    console.log(args);

    if (args.length === numArgs){
      return fn.apply(null, args);
    }
    else{
      return _curry;
    }
  };
};
