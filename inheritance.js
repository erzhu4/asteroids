Function.prototype.inherits = function(superClass){
  var Surrogate = function () {};
  Surrogate.prototype = superClass.prototype;
  this.prototype = new Surrogate();
};







function MovingObject () {};

MovingObject.prototype.fly = function(){
  return "fly";
};

function Ship () {};
Ship.inherits(MovingObject);

function Asteroid () {};
Asteroid.inherits(MovingObject);

Asteroid.prototype.blowUp = function(){
  return "explode";
};
