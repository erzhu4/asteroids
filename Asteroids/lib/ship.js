(function() {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }



  var Ship = Asteroids.Ship = function(pos, game){
    Ship.RADIUS = 20;
    Ship.COLOR = "#858585";
    Ship.VEL = [0,0];
    window.Asteroids.MovingObject.call(this, pos, Ship.VEL, Ship.RADIUS, Ship.COLOR, game);
  };

  window.Asteroids.Util.inherits(Ship, window.Asteroids.MovingObject);

  Ship.prototype.relocate = function(){
    this.pos = Asteroids.Game.randomPosition();
    this.vel = Ship.VEL;
  };

  Ship.prototype.power = function(impulse){
    this.vel[1] = impulse[1];
    this.vel[0] = impulse[0];
  };

  Ship.prototype.fireBullet = function(){
    var bullet = new Asteroids.Bullet(this.game);
    game.bullets.push(bullet);
    game.allObjects.push(bullet);
  };

})();
