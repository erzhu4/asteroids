(function() {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var Game = Asteroids.Game = function() {
    Game.DIM_X = 1000;
    Game.DIM_Y = 600;
    Game.NUM_ASTEROIDS = 10;
    this.asteroids = [];
    this.bullets = [];
    Game.addAsteroids.bind(this)();
    Game.addShip.bind(this)();
    this.allObjects = this.asteroids.concat(this.ship);
  };

  Game.addAsteroids = function(){
    for (var i = 0; i < Game.NUM_ASTEROIDS; i++){
      var nwas = new Asteroids.Asteroid(Game.randomPosition(), Game.randomVelocity(), this);
      this.asteroids.push(nwas);
    }
  };

  Game.addShip = function(){
    this.ship = new Asteroids.Ship(Game.randomPosition(), this);
  };

  Game.randomPosition = function(){
    return [Math.floor(Math.random() * 1000), Math.floor(Math.random() * 300)];
  };

  Game.randomVelocity = function(){
    return [Math.floor(Math.random() * 3 - 1), Math.floor(Math.random() * 3 - 1)];
  }

  Game.prototype.draw = function(ctx){
    ctx.clearRect(0, 0, Game.DIM_X + 200, Game.DIM_Y + 200);

    for (var i = 0; i < this.allObjects.length; i++){
      this.allObjects[i].draw(ctx);
    }
  };

  Game.prototype.moveObjects = function(){
    for (var i = 0; i < this.allObjects.length; i++){
      this.allObjects[i].move();
    }
  };

  Game.prototype.wrap = function(pos){
    var wrappedPos = pos;
    var x = pos[0];
    var y = pos[1];

    if (x > Game.DIM_X){
      wrappedPos[0] = 0;
    }else if (x < 0){
      wrappedPos[0] = Game.DIM_X;
    }else if (y > Game.DIM_Y){
      wrappedPos[1] = 0;
    }
    else if (y < 0){
      wrappedPos[1] = Game.DIM_Y;
    }

    return wrappedPos;
  };

  Game.prototype.checkCollisions = function(){
    for (var i = 0; i < this.allObjects.length - 1; i++){
      for (var j = i + 1; j < this.allObjects.length; j++){
        if (this.allObjects[i].isCollidedWith(this.allObjects[j])){
          //alert("COLLIDED");
            this.allObjects[i].asteroidCollideWith(this.allObjects[j]);

        }
      }
    }
  };

  Game.prototype.step = function(){
    this.moveObjects();
    this.checkCollisions();
  };


  Game.prototype.remove = function(asteroid){
    var idx = this.allObjects.indexOf(asteroid);
    var left = this.allObjects.slice(0, idx);
    var right = this.allObjects.slice(idx + 1);
    this.allObjects = left.concat(right);
  };



})();
