(function() {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }



  var Asteroid = Asteroids.Asteroid = function(pos, vel, game){
    Asteroid.COLOR = "#00FF00";
    Asteroid.RADIUS = 30;
    window.Asteroids.MovingObject.call(this, pos, vel, Asteroid.RADIUS, Asteroid.COLOR, game);
  };

  window.Asteroids.Util.inherits(Asteroid, window.Asteroids.MovingObject);

  Asteroid.prototype.asteroidCollideWith = function(otherObject){
    if (otherObject instanceof Asteroids.Ship){
      otherObject.relocate();
    }
  };


})();
