(function() {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var Bullet = Asteroids.Bullet = function(game){
    Bullet.RADIUS = 5;
    Bullet.COLOR = "#858585";
    Bullet.VEL = [20,0];
    window.Asteroids.MovingObject.call(this, game.ship.pos, Bullet.VEL, Bullet.RADIUS, Bullet.COLOR, game);
  };

  window.Asteroids.Util.inherits(Bullet, window.Asteroids.MovingObject);

})();
