(function() {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var GameView = Asteroids.GameView = function(){
    this.game = new Asteroids.Game();
    GameView.start.bind(this)();
  };

  GameView.start = function(){
    var ctx = canvasEl.getContext("2d");
    window.setInterval((function () {
     this.bindKeyHandlers();
     this.game.step();
     this.game.draw(ctx);
    }).bind(this), 20);
  };

  GameView.prototype.bindKeyHandlers = function(){

    key('w', function(){this.game.ship.power([0, -1]);
    }.bind(this));

    key('s', function(){this.game.ship.power([0, 1]);
    }.bind(this));

    key('d', function(){this.game.ship.power([1, 0]);
    }.bind(this));

    key('a', function(){this.game.ship.power([-1, 0]);
    }.bind(this));

    key('p', function(){this.game.ship.fireBullet();
    }.bind(this));

  };


})();
