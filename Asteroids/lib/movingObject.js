(function() {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }



  var MovingObject = Asteroids.MovingObject = function(pos, vel, radius, color, game){
    this.pos = pos;
    this.vel = vel;
    this.radius = radius;
    this.color = color;
    this.game = game;
  };

  MovingObject.prototype.draw = function(ctx){
    ctx.fillStyle = this.color;
    ctx.beginPath();

    ctx.arc(
      this.pos[0],
      this.pos[1],
      this.radius,
      0,
      2 * Math.PI,
      false
    );

    ctx.fill();
  };

  MovingObject.prototype.move = function(){
    this.pos = this.game.wrap(this.pos);
    this.pos[0] += this.vel[0];
    this.pos[1] += this.vel[1];
  };

  MovingObject.prototype.isCollidedWith = function(otherObject){
    var xsqrd = (this.pos[0] - otherObject.pos[0]) * (this.pos[0] - otherObject.pos[0]);
    var ysqrd = (this.pos[1] - otherObject.pos[1]) * (this.pos[1] - otherObject.pos[1]);
    var dis = Math.sqrt(xsqrd + ysqrd);
    if (dis < this.radius + otherObject.radius){
      return true;
    }
    return false;
  };

  MovingObject.prototype.collideWith = function(otherObject){
    
  };


})();

//Dist([x_1, y_1], [x_2, y_2]) = sqrt((x_1 - x_2) ** 2 + (y_1 - y_2) ** 2)
